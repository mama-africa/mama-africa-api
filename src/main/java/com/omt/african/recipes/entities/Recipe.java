package com.omt.african.recipes.entities;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "recipe")
@Data
public class Recipe {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "recipe_id", updatable = false, nullable = false)
	private UUID recipeId;

	@Column(name = "recipe_name")
	private String recipeName;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	private List<Ingredients> ingredients;

	@Column(name = "cooking_method")
	private String cookingMethod;

	@Column(name = "video_url")
	private String videoUrl;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	private List<RecipePhoto> recipePhoto;

	@ManyToOne(fetch = FetchType.LAZY)
	private User author;

	public Recipe() {

	}

	public Recipe(UUID recipeId, String recipeName, List<Ingredients> ingredients, String cookingMethod,
			String videoUrl, List<RecipePhoto> imageUrl) {

		recipeId = this.recipeId;
		recipeName = this.recipeName;
		ingredients = this.ingredients;
		cookingMethod = this.cookingMethod;
		videoUrl = this.videoUrl;
		imageUrl = this.recipePhoto;

	}

}
