package com.omt.african.recipes.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;


@Entity
@Data
@Table(name="recipe_photo")
public class RecipePhoto {
	
	
	@Id
	@GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "image_id", updatable = false, nullable = false)
	private UUID imageId;
	
	
	@Column(name="image_url")
	private String imageUrl;
	
	
	

	public RecipePhoto() {
		
	}
	
	public RecipePhoto(UUID imageId, String imageUrl) {
		super();
		this.imageId = imageId;
		this.imageUrl = imageUrl;
	} 
	
	
}
