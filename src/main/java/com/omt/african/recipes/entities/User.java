package com.omt.african.recipes.entities;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "user_id", updatable = false, nullable = false)
	private UUID userId;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "user_image_url")
	private String userImageUrl;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	@Column(name = "active")
	private boolean active;

	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	private List<Recipe> recipes;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_for_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	@JsonIgnoreProperties("users")
	private List<UserRole> roles;

	public User(String firstName, String lastName, String userImageUrl, String email, String password, boolean active,
			List<UserRole> roles) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userImageUrl = userImageUrl;
		this.email = email;
		this.password = password;
		this.active = active;
		this.roles = roles;
	}

}
