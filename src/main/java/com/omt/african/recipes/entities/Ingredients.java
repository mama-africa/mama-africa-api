package com.omt.african.recipes.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Data
@Table(name="ingredients")
public class Ingredients {
	
	@Id
	@GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "ingredients_id", updatable = false, nullable = false)
	private UUID ingredientId;
	
	@Column(name="ingredient_name")
	private String ingredientName; 
	
	@Column(name="ingredient_quantity")
	private String IngredientQuantity; 
	
	

	public Ingredients(UUID ingredientId, String ingredientName, String ingredientQuantity) {
		super();
		this.ingredientId = ingredientId;
		this.ingredientName = ingredientName;
		IngredientQuantity = ingredientQuantity;
	}

	public Ingredients() {
		super();
	} 
	
	

}
