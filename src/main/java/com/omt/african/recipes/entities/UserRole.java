package com.omt.african.recipes.entities;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user_role")
public class UserRole {
	
	
	
	
	@Id
	@GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "role_id", updatable = false, nullable = false)
	private UUID roleId;
	
	@Column(name="role", unique = true)
	private String role; 
	
	
	@Column(name="role_description")
	private String roleDescription; 
	
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="user_for_roles", joinColumns = @JoinColumn(name="role_id"), inverseJoinColumns = @JoinColumn(name="user_id"))
	@JsonIgnoreProperties("roles")
	private List<User> users;
	
	public UserRole(String theRole, String theRoledescription) {
		
		role=theRole;
		roleDescription=theRoledescription;
		
	}
}
