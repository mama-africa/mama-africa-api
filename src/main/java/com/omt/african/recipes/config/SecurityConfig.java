package com.omt.african.recipes.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.omt.african.recipes.services.MyUserDetailsServices;

@Configuration
@EnableWebSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter{

	
	@Autowired
	private MyUserDetailsServices myUserDetailsServices;
	@Autowired
	private JwtRequestFilter theJwtRequestFilter;
	
	
	public SecurityConfig(MyUserDetailsServices userDetailsServices, JwtRequestFilter jwtRequestFilter) {
		
		myUserDetailsServices=userDetailsServices;
		theJwtRequestFilter = jwtRequestFilter;
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		
		auth.userDetailsService(myUserDetailsServices);
	}
	
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.authorizeRequests()
		.antMatchers("/api/authenticate").permitAll()
		.antMatchers(HttpMethod.POST, "/api/users").permitAll()
		.antMatchers(HttpMethod.GET, "/api/users").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET, "/api/users/**").hasAnyRole("ADMIN", "USER")
		.antMatchers(HttpMethod.DELETE, "/api/users/**").hasAnyRole("ADMIN", "USER")
		.and().cors()
		.and()
			.csrf()
			.disable();
		http.addFilterBefore(theJwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
}
