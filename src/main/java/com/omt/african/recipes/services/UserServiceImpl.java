package com.omt.african.recipes.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omt.african.recipes.dao.UserDAO;
import com.omt.african.recipes.dao.UserRoleDAO;
import com.omt.african.recipes.entities.User;
import com.omt.african.recipes.entities.UserRole;
import com.omt.african.recipes.exceptions.RessourceAlreadyExistException;
import com.omt.african.recipes.exceptions.RessourceNotFoundException;

@Service
public class UserServiceImpl implements UserService {

	private UserDAO userDao;
	private UserRoleDAO userRoleDao;

	@Autowired
	public UserServiceImpl(UserDAO theUserDAO, UserRoleDAO theRoleDAO) {
		userDao = theUserDAO;
		userRoleDao = theRoleDAO;
	}

	@Override
	public User getUser(UUID userId) {
		Optional<User> result = userDao.findById(userId);
		result.orElseThrow(() -> new RessourceNotFoundException("No user with this ID--" + userId));
		User user = result.get();

		return user;
	}

	@Override
	public List<User> getUsers() {

		return userDao.findAll();
	}

	@Override
	public void createUser(User user) {

		System.out.println(" before done");
		Optional<UserRole> result = userRoleDao.findByRole("ROLE_USER");
		result.orElseThrow(() -> new RessourceNotFoundException("User Role not found"));
		UserRole userRole = result.get();

		List<UserRole> roles = user.getRoles();
		roles.add(userRole);
		user.setRoles(roles);
		userDao.save(user);
		System.out.println("done");

	}

	@Override
	public User updateUser(User user, UUID userId) {
		user.setUserId(userId);
		userDao.save(user);
		return user;
	}

	@Override
	public void deleteUser(UUID userId) {
		User user = getUser(userId);

		userDao.delete(user);

	}

	@Override
	public void changeStatus(UUID userId) {
		User user = getUser(userId);
		if (user.isActive()) {
			user.setActive(false);
		} else {
			user.setActive(true);
		}

		userDao.save(user);
	}

	@Override
	public User manageRole(UUID userId, String role, String action) {

		User user = getUser(userId);

		Optional<UserRole> result = userRoleDao.findByRole(role);
		result.orElseThrow(() -> new RessourceNotFoundException("this role doesn't exist"));
		UserRole userRole = result.get();

		List<UserRole> userRoles = user.getRoles();

		for (UserRole usrRole : userRoles) {

			if (usrRole.getRole().equals(role) && action.equals("add")) {

				throw new RessourceAlreadyExistException("this User Already have this role");

			} else if (usrRole.getRole().equals(role) && action.equals("remove")) {

				userRoles.remove(userRole);
				user.setRoles(userRoles);

				return user;

			}

		}

		userRoles.add(userRole);

		user.setRoles(userRoles);

		userDao.save(user);

		return user;
	}

	@PostConstruct
	public void populateRoles() {

		UserRole userRole = new UserRole("ROLE_USER", "Default Role of a User");
		UserRole adminRole = new UserRole("ROLE_ADMIN", "the Role needed to manage all other users");

		userRoleDao.save(userRole);
		userRoleDao.save(adminRole);

		List<UserRole> list = new ArrayList<UserRole>();

		User user = new User("Oscar", "Tematio", "google.com", "omtematio@gmail.com",
				"$2y$12$1wSYykCEIhwdkTL3GuFdD.pFwPUMygRwLdhYavlpEvDqu.8Kyv4mW", true, list);
		List<UserRole> userRoles = user.getRoles();
		userRoles.add(userRoleDao.findByRole("ROLE_USER").get());
		userRoles.add(userRoleDao.findByRole("ROLE_ADMIN").get());

		user.setRoles(userRoles);
		userDao.save(user);
	}

}
