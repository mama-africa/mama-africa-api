package com.omt.african.recipes.services;

import java.util.List;
import java.util.UUID;

import com.omt.african.recipes.entities.Recipe;

public interface RecipesServices {
	
	public List<Recipe> getRecipes();
	public Recipe getRecipe(UUID recipeId);
	public void addRecipe(Recipe recipe);
	public Recipe updateRecipe(Recipe recipe, UUID recipeId);
	public void deleteRecipe(UUID recipeId);

}
