package com.omt.african.recipes.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.omt.african.recipes.dao.UserDAO;
import com.omt.african.recipes.entities.User;
import com.omt.african.recipes.exceptions.RessourceNotFoundException;
import com.omt.african.recipes.model.UserDetailsModel;

@Service
public class MyUserDetailsServices implements UserDetailsService {
	
	private UserDAO userDao; 
	
	
	@Autowired
	public MyUserDetailsServices(UserDAO theUserDAO) {
		
		userDao=theUserDAO;
	 }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<User> result =  userDao.findByEmail(username);
		
		result.orElseThrow(()->new RessourceNotFoundException("No user with this Email-- "+username));
		
		UserDetailsModel userDetails = new UserDetailsModel(result.get());
		
		return userDetails;
	}

}
