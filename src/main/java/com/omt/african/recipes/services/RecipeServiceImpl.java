package com.omt.african.recipes.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.omt.african.recipes.dao.RecipeDAO;
import com.omt.african.recipes.entities.Recipe;
import com.omt.african.recipes.exceptions.RessourceNotFoundException;


@Service
public class RecipeServiceImpl implements RecipesServices {
	
	
	private RecipeDAO theRecipeDAO; 
	
	@Autowired
	public RecipeServiceImpl(RecipeDAO recipeDAO) {
		theRecipeDAO= recipeDAO;
	}
	
	@Override
	public List<Recipe> getRecipes() {
		
		return theRecipeDAO.findAll(Sort.by(Sort.Direction.ASC, "recipeName"));
	}

	@Override
	public Recipe getRecipe(UUID recipeId) {
		
		Optional<Recipe> result = theRecipeDAO.findById(recipeId);
		result.orElseThrow(()-> new RessourceNotFoundException("No recipe with this Id -- "+ recipeId));
		
		Recipe recipe = result.get();

		
		return recipe;
	}

	@Override
	public void addRecipe(Recipe recipe) {
		
		theRecipeDAO.save(recipe);
		
	}
	

	@Override
	public Recipe updateRecipe(Recipe recipe, UUID recipeId) {
		recipe.setRecipeId(recipeId);
		
		theRecipeDAO.save(recipe);
		
		return recipe;
	}

	@Override
	public void deleteRecipe(UUID recipeId) {
		Optional<Recipe> result = theRecipeDAO.findById(recipeId);
		result.orElseThrow(()-> new RessourceNotFoundException("No recipe with this Id -- "+ recipeId));
		
		Recipe recipe = result.get();
		
		theRecipeDAO.delete(recipe);
	}

}
