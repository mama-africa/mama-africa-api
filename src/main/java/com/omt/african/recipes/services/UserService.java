package com.omt.african.recipes.services;

import java.util.List;
import java.util.UUID;

import com.omt.african.recipes.entities.User;

public interface UserService {
	
	public User getUser( UUID userID);
	public List<User> getUsers();
	public void createUser(User user);
	public User updateUser(User user, UUID userId);
	public void deleteUser(UUID userId);
	public void changeStatus(UUID userId);
	public User manageRole(UUID userId, String role, String action);
	

}
