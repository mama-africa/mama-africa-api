package com.omt.african.recipes.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omt.african.recipes.model.AuthenticationRequestModel;
import com.omt.african.recipes.model.AuthenticationResponseModel;
import com.omt.african.recipes.services.JwtUtilService;
import com.omt.african.recipes.services.MyUserDetailsServices;

@RestController
@RequestMapping("/api")
public class AuthenticationRestController {

	private AuthenticationManager theAuthenticationManager;
	private MyUserDetailsServices theUserDetailsService;
	private JwtUtilService theJwtUtilService;

	@Autowired
	public AuthenticationRestController(AuthenticationManager authenticationManager, MyUserDetailsServices myUserDetailsServices, JwtUtilService jwtUtilService
			) {
		theAuthenticationManager = authenticationManager;
		theUserDetailsService = myUserDetailsServices;
		theJwtUtilService = jwtUtilService;
	}

	@PostMapping("/authenticate")
	public ResponseEntity<?> createAuthenticationToken(
			@RequestBody AuthenticationRequestModel authenticationRequestModel) throws Exception {

		try {
			theAuthenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequestModel.getUsername(), authenticationRequestModel.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect username or password", e);

		}
		final UserDetails userDetails = theUserDetailsService.loadUserByUsername(authenticationRequestModel.getUsername());
		
		final String jwt = theJwtUtilService.generateToken(userDetails);
		
		return ResponseEntity.ok(new AuthenticationResponseModel(jwt));
		
	}

}
