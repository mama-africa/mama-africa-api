package com.omt.african.recipes.rest;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omt.african.recipes.entities.User;
import com.omt.african.recipes.model.RequestResponse;
import com.omt.african.recipes.services.UserService;

@RestController
@RequestMapping("/api")
public class UserRestController {
	
	private UserService  userService;
	
	@Autowired
	public UserRestController(UserService theUservice) {
		
		userService=theUservice;
		
	}
	
	@GetMapping("/users")
	public List<User> getUsers() {
		
		return userService.getUsers();
	}

	@GetMapping("/users/{userId}")
	public User getUser(@PathVariable UUID userId) {
		return userService.getUser(userId);
	}
	
	@PostMapping("/users")
	public RequestResponse createUSer( @RequestBody User user) {
		userService.createUser(user);
		
		return new RequestResponse("User succesfully Added");
	}
	
	
	@PutMapping("/users/{userId}")
	public User updateUSer(@RequestBody User user , @PathVariable UUID userId) {
		
		return userService.updateUser(user, userId);
		
	}
	
	@DeleteMapping("/users/{userId}")
	public RequestResponse delteUser(@PathVariable UUID userId) {
		userService.deleteUser(userId);
		return new RequestResponse("User Deleted");
	}
	
	@PutMapping("/users/changestatus/{userId}")
	public RequestResponse blockUser(@PathVariable UUID userId) {
		
		userService.changeStatus(userId);
		
		return new RequestResponse("Status Changed");
		
	}
	@PutMapping("/users/{userId}/addrole/{role}")
	public User addRole(@PathVariable UUID userId, @PathVariable String role) {
		
		return userService.manageRole(userId, role, "add");
		
	}
	
	@PutMapping("/users/{userId}/removerole/{role}")
	public User removeRole(@PathVariable UUID userId, @PathVariable String role) {
		
		return userService.manageRole(userId, role, "remove");
		
	}
}
