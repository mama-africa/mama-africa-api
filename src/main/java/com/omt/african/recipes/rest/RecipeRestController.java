package com.omt.african.recipes.rest;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omt.african.recipes.entities.Recipe;
import com.omt.african.recipes.model.RequestResponse;
import com.omt.african.recipes.services.RecipesServices;

@RestController
@RequestMapping("/api")
public class RecipeRestController {

	
	private RecipesServices recipeService;
	
	@Autowired
	RecipeRestController(RecipesServices theRecipeService) {
		
		recipeService= theRecipeService;
	}
	
	@GetMapping("/recipes")
	public List<Recipe> getRecipes() {
		
		return recipeService.getRecipes();
		
	}
	
	@GetMapping("/recipes/{recipeId}")
	public Recipe getRecipe(@PathVariable UUID recipeId) {
		
		return recipeService.getRecipe(recipeId);
		
	}
	
	@PostMapping("/recipes")
	public RequestResponse createRecipes(@RequestBody Recipe recipe) {
		
		recipeService.addRecipe(recipe);
		
		return new RequestResponse("Succesfully Added");
		
	}
	
	
	@PutMapping("/recipes/{recipeId}")
	public Recipe updateRecipe(@PathVariable UUID recipeId, @RequestBody Recipe recipe) {
		
		return recipeService.updateRecipe(recipe, recipeId);
	}
	
	
	@DeleteMapping("/recipes/{recipeId}")
	public RequestResponse deleteRecipe(@PathVariable UUID recipeId) {
		
		recipeService.deleteRecipe(recipeId);
		
		return new RequestResponse("Succesfully deleted");
	}
}
