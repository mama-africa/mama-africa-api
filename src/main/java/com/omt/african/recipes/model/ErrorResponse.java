package com.omt.african.recipes.model;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class ErrorResponse {
	private int status;

	private String message;
	
	private long timeStamp;
	
	public ErrorResponse() {
		
	}

}
