package com.omt.african.recipes.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.omt.african.recipes.entities.User;
import com.omt.african.recipes.entities.UserRole;

public class UserDetailsModel implements UserDetails {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private String email;
	private String password;
	private boolean active;
	private List<GrantedAuthority> roles;

	public UserDetailsModel(User user) {

		email = user.getEmail();
		password = user.getPassword();
		active = user.isActive();
		roles = Arrays.stream(getRoles(user.getRoles())).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return active;
	}

	@Override
	public boolean isAccountNonLocked() {
		return active;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return active;
	}

	@Override
	public boolean isEnabled() {
		return active;
	}

	public String[] getRoles(List<UserRole> authorities) {
		
		ArrayList<String> roles = new ArrayList<String>();

		for (UserRole authority : authorities) {

			roles.add(authority.getRole());

		}

		String[] arrayRoles = new String[roles.size()];
		return roles.toArray(arrayRoles);
	}

}
