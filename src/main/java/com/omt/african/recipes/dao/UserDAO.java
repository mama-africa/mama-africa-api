package com.omt.african.recipes.dao;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.omt.african.recipes.entities.User;

public interface UserDAO extends JpaRepository<User, UUID> {
	
	Optional<User> findByEmail(String email);

}
