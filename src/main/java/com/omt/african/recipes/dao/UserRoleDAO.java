package com.omt.african.recipes.dao;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.omt.african.recipes.entities.UserRole;

public interface UserRoleDAO extends JpaRepository<UserRole, UUID>{

	Optional<UserRole> findByRole(String role);
}
