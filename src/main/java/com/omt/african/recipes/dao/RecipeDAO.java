package com.omt.african.recipes.dao;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.omt.african.recipes.entities.Recipe;

@Repository
public interface RecipeDAO extends JpaRepository<Recipe, UUID> {

}
