package com.omt.african.recipes.exceptions;

public class RessourceNotFoundException extends RuntimeException  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RessourceNotFoundException() {
			
	}

	public RessourceNotFoundException(String message) {
		super(message);
	}

	public RessourceNotFoundException(Throwable cause) {
		super(cause);
	}

	public RessourceNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public RessourceNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
