package com.omt.african.recipes.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.omt.african.recipes.model.ErrorResponse;

@ControllerAdvice
public class RessourceExceptionHandler {
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> notFoundException(RessourceNotFoundException exc) {

		// create EmployeeErrorResponse

		ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.value(), exc.getMessage(),
				System.currentTimeMillis());

		// return ResponseEntity

		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> alreadyExistException(RessourceAlreadyExistException exc) {

		ErrorResponse error = new ErrorResponse(HttpStatus.CONFLICT.value(), exc.getMessage(),
				System.currentTimeMillis());

		return new ResponseEntity<ErrorResponse>(error, HttpStatus.CONFLICT);
	}
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> invalidTokenException(InvalidTokenException exc) {

		ErrorResponse error = new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), exc.getMessage(),
				System.currentTimeMillis());

		return new ResponseEntity<ErrorResponse>(error, HttpStatus.UNAUTHORIZED);
	}

}
