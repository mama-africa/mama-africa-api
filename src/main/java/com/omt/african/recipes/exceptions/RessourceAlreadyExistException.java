package com.omt.african.recipes.exceptions;

public class RessourceAlreadyExistException  extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public RessourceAlreadyExistException() {
		
	}

	public RessourceAlreadyExistException(String message) {
		super(message);
	}

	public RessourceAlreadyExistException(Throwable cause) {
		super(cause);
	}

	public RessourceAlreadyExistException(String message, Throwable cause) {
		super(message, cause);
	}

	public RessourceAlreadyExistException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
